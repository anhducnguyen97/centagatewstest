package com.securemetric;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class SecureRestClientTrustManager {

	@SuppressWarnings("finally")
	public String calculateHmac256(String secret,String message)
    {
        String hash = null;
        try {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);

        hash = Hex.encodeHexString(sha256_HMAC.doFinal(message.getBytes()));
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally
        {    
            return hash;
        }
    }
	
	public static void main(String[] agrs) {
		SecureRestClientTrustManager clientTrustManager = new SecureRestClientTrustManager();
		String secretKey = "p98rjXh03qHX";
		String username = "testadmin";
		String password = "123456a@A";
		String integrationKey = "ee9228e350b10b744aff1135e01f3d1c858bcd25e84dd7038946bdb73144653d";
		String unixTimestamp = "1637638462";
		String message = username + password + integrationKey + unixTimestamp;
		
		String hashValue = clientTrustManager.calculateHmac256(secretKey, message);
		System.out.println(hashValue);
		
		String authToken = "uqfBz9HYCWSLLzTq1efcvbX05KtV5J3iLV6IwM/NFSs=";
		String secretCode = "klV4KFzQ2u69bqOPRpfgn1YVf7lhWpkB";
		String message2 = username + authToken;
		
		
		String cenToken = clientTrustManager.calculateHmac256(secretCode, message2);
		System.out.println(cenToken);
		
	}
}
